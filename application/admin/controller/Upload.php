<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/6 0006
 * Time: 13:34
 */

namespace app\admin\controller;


use think\Config;
use think\Controller;

class Upload extends Controller
{
    /**
     * @Notes:上传图片
     * @Interface image
     * @return string|\think\response\Json
     * @author: Chenrui
     * @Time: 2018/9/6 0006   13:50
     */
    public function image(){
        $file=$this->request->file("file");
        if($file){
            $info=$file->move("../public/static/upload");
            if(!$info){
                return json([
                    "code"  =>3000,
                    "error"   =>$file->getError()
                ],200);
            }else{
                return json([
                    "code"  =>200,
                    "msg"   =>"ok",
                    "data"  =>["url" =>Config::get("base.base_url").'/upload/'.$info->getSaveName()]
                ],200);
            }
        }else{
            return "";
        }
    }
}