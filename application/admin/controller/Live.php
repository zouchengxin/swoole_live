<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/6 0006
 * Time: 19:51
 */

namespace app\admin\controller;


use app\common\Syn_redis;
use app\common\Asyn_mysql;
use think\Config;
use think\Controller;

class Live extends Controller
{
    public function push(){
        $ws=$_SERVER["ws_server"];

        //异步mysql入库
        $data=request()->post();
        $data['create_time']=time();
        $db=Asyn_mysql::getInstance();
        $sql='insert into live_outs(`team_id`,`content`,`image`,`type`,`create_time`)'
            ." values({$data['team_id']},"
            ."'"."{$data['content']}"."'"
            .",'"."{$data['image']}"."'"
            .",{$data['type']},{$data['create_time']})";

        $db->query($sql);

        /*$member=Syn_redis::getInstance()->SMEMBERS(Config::get("syn_redis.user"));

        foreach ($member as $fd){
            $str=json_encode($data);
            $ws->push($fd,$str);
        }*/

        $port=$ws->ports[1];
        foreach($port->connections as $_fd){
            if($ws->exist($_fd)){
                $ws->push($_fd,$data);
            }
        }

    }

}