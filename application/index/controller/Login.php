<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/4 0004
 * Time: 10:25
 */

namespace app\index\controller;


use think\Controller;

class Login extends Controller
{
    /**
     * @Notes:
     * @Interface index
     * @return \think\response\Json
     * @author: Chenrui
     * @Time: 2018/9/4 0004   11:01
     */
    public function index(){
        $phone_num=(int)$_GET['phone_num'];
        if(!$phone_num){
            return json([
                "code"  =>3000,
                "msg"   =>"手机号不能为空"
            ],200);
        }

        $code=rand(10000,99999);
        $redis=new \Swoole\Coroutine\Redis();
        $redis->connect("127.0.0.1",6379);
        $res=$redis->setex("num_{$phone_num}",60,$code);
        if($res){
            return json([
                "code"  =>200,
                "data"  =>["code"   =>$code]
            ],200);
        }
    }

}