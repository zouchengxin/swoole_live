<?php
namespace app\index\controller;

use app\common\Syn_redis;
use think\Controller;

class Index extends Controller
{
    public function home()
    {
       $data=$_GET;
       if(!$data['phone_num']||!$data['code']){
           return json([
               "code"   =>2000,
               "msg"    =>"手机号或验证码为空"
           ],200);
       }
       $syn_redis=Syn_redis::getInstance();
       $code=$syn_redis->get("num_".$data['phone_num']);
        if($data['code']==$code){
            return json([
                "code"  =>200,
                "msg"   =>"ok"
            ],200);
        }else{
            return json([
                "code"  =>3000,
                "msg"   =>"验证码错误"
            ],200);
        }
    }

    public function index(){
        return "";
    }
}
