<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/7 0007
 * Time: 19:04
 */

namespace app\common;


use think\Config;

class Asyn_mysql
{
    private $mysql;
    private static $instance;
    private $config;
    public static function getInstance(){
        if(empty(self::$instance)){
            self::$instance=new self();
        }
        return self::$instance;
    }

    private function __construct($config=[]){
        $this->config = array_merge(Config::get("mysql"), $config);
        $this->mysql = new \Swoole\Coroutine\MySQL();
        if(!$this->mysql->connect($this->config)){
            echo "mysql-error:{$this->mysql->error}\n";
        }
    }

    public function query($sql){
        $res=$this->mysql->query($sql);
         if(!$res){
             echo "mysql-error:{$this->mysql->error}\n";
         }else{
             echo "mysql:".$res.PHP_EOL;
         }
    }
}