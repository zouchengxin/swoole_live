<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/4 0004
 * Time: 12:53
 */

namespace app\common;
use think\Config;

/**
 * 同步Redis
 * Class Syn_redis
 * @package app\common
 */
class Syn_redis
{
    private  $redis;
    private static $instance;
    public static function getInstance(){
        if(empty(self::$instance)){
            self::$instance=new self();
        }
        return self::$instance;
    }

    private function __construct()
    {
        $this->redis=new \Redis();
        $res=$this->redis->connect(Config::get("syn_redis.host"),Config::get("syn_redis.port"));
        if(!$res){
            throw new \Exception(["连接redis失败"],500);
        }
    }

    public  function set($key,$val,$timeout=0){
        if($timeout!=0){
            return $this->redis->setex($key,$timeout,$val);
        }else{
            return $this->redis->set($key,$val);
        }
    }

    public  function get($key){
        return $this->redis->get($key);
    }

    public function __call($name, $arguments)
    {
        // TODO: Implement __call() method.
        return $this->redis->$name(...$arguments);
    }

}