<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/9/2 0002
 * Time: 13:30
 */

$http=new swoole_http_server("0.0.0.0",9505);

$http->set([
    'document_root' => __DIR__.'/../public/static',
    'enable_static_handler' => true,
    'worker_num'   =>8
]);

$http->on("request",function ($req,$res) use($http){
    // 1. 加载基础文件
    //require_once __DIR__ . '/../thinkphp/base.php';

    $_SERVER=[];$_GET=[];$_POST=[];
    if(isset($req->server)){
	foreach($req->server as $key =>$val){
	    $_SERVER[strtoupper($key)]=$val;
	}
    }

    if(isset($req->header)){
	foreach($req->header as $key =>$val){
	    $_SERVER[strtoupper($key)]=$val;
	}
    }

    if(isset($req->get)){
        foreach($req->get as $key =>$val){
            $_GET[$key]=$val;
        }
    }

    if(isset($req->post)){
        foreach($req->post as $key =>$val){
            $_POST[$key]=$val;
        }
    }

    var_dump($_GET);
    var_dump($_POST);

    //打开输出缓冲
    ob_start();
    // 2. 执行应用
    think\App::run()->send();
    $data=ob_get_contents();
    ob_end_clean();

    $res->end($data);
    var_dump(think\Request::instance());
    //$http->close();
});

$http->on("WorkerStart",function ($server,$worker_id){
    // 定义应用目录
    define('APP_PATH', __DIR__ . '/../application/');
    // 1. 加载基础文件
    require __DIR__ . '/../thinkphp/base.php';
});

$http->start();
