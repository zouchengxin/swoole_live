$(function () {
    var ws=new WebSocket("ws://39.106.207.193:9506");
    ws.onopen=function (res) {
        console.log("open:"+res);
    }

    ws.onmessage=function (res) {
        var data=JSON.parse(res.data);
        console.log("message:"+data);
        $("#comments").append("<div class='comment'>" +
            "<span>"+res.user+"</span>" +
            "<span>"+res.content+"</span>" +
            "</div>");
    }

    ws.onerror=function (e) {
        console.log("error:"+e);
    }

    ws.onclose=function (res) {
        console.log("close:"+res);
    }

    var strCookie=document.cookie;
    var phone_num=strCookie.substring(strCookie.indexOf("="+1));
    $("#send").keydown(function (e) {
        if(e.keyCode==13){//按下回车键
            let content=$(this).val();
            let data={
                "content":content,
                "user":"用户"+phone_num
            }
            data=JSON.stringify(data);
            ws.send(data);
        }
    });
});